from django.db import models


class Pedido(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    total = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    datapedido = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pedidos'


class Produto(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    nome = models.CharField(max_length=50)
    preco = models.DecimalField(max_digits=10, decimal_places=2)
    estoque_qty = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'produtos'


class PedidoProduto(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    quantidade = models.IntegerField()
    id_pedido = models.ForeignKey(Pedido, models.DO_NOTHING, db_column='id_pedido')
    id_produto = models.ForeignKey(Produto, models.DO_NOTHING, db_column='id_produto')

    class Meta:
        managed = False
        db_table = 'pedidos_x_produtos'

