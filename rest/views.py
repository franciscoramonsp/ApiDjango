from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.db import IntegrityError
import json
from datetime import datetime
from rest.models import Produto, Pedido, PedidoProduto


@csrf_exempt
def produtos(request):
    if not auth(request):
        return HttpResponse(status="403")

    data = get_data(request)
    produtos = []


    if request.method == "GET":
        if data:
            produtos = list(Produto.objects.filter(**data).values())
        else:
            produtos_qs = list(Produto.objects.raw("SELECT * FROM pedidos"))
            for p in produtos_qs:
                produtos.append(object_to_dict(p))

        return JsonResponse(data=produtos, safe=False, status=200)

    elif request.method == "POST":
        produto = Produto(**data)
        try:
            produto.save()
        except IntegrityError as error:
            return JsonResponse(data=error, safe=False, status=400)
        return JsonResponse(data=object_to_dict(produto), safe=False, status=200)


@csrf_exempt
def pedidos(request):
    if not auth(request):
        return HttpResponse(status="403")

    data = get_data(request)

    if request.method == "GET":
        if request.GET.get('de'):
            filtro_pedidos = busca_pedido_em_periodo(request.GET.get('de'),request.GET.get('ate'))
        else:
            filtro_pedidos = busca_pedidos(data)

        return JsonResponse(data=filtro_pedidos, safe=False, status=200)

    elif request.method == "POST":
        pedido = Pedido(**data)
        try:
            pedido.save()
        except IntegrityError as error:
            return JsonResponse(data=error, safe=False, status=400)
        return JsonResponse(data=object_to_dict(pedido), safe=False, status=200)


# Helps

def busca_pedido_em_periodo(de,ate):
    if ate is None:
        return busca_pedidos({'datapedido__range': (datetime.strptime(de,"%Y-%m-%d"), datetime.now())})
    return busca_pedidos({'datapedido__range':(datetime.strptime(de,"%Y-%m-%d"),ate)})

def busca_pedidos(data):
    print(data)
    if data:
        pedidos = Pedido.objects.filter(**data).values()
    else:
        pedidos = Pedido.objects.all().values()
    result = []
    for p in list(pedidos):
        result.append({"pedido": p, "itens": list(PedidoProduto.objects.filter(id_pedido=p["id"]).values())})
    return result


def auth(request):
    if 'HTTP_AUTHORIZATION' in request.META:
        if request.META['HTTP_AUTHORIZATION'] == "dkdfw3k2hkd3kIHKL93kl8782":
            return True
    return False


def get_data(request):
    if request.body:
        return json.loads(request.body.decode('utf-8'))
    else:
        return None

def object_to_dict(o):
    dict = o.__dict__
    print(dict)
    dict.__delitem__("_state")
    return dict