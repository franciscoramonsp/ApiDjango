from django.urls import path,include
from . import views

urlpatterns = [
    path('/produtos/', views.produtos),
    path('/pedidos/', views.pedidos),
]
