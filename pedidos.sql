CREATE DATABASE 'django.fdb';
CONNECT 'django.fdb';

CREATE TABLE produtos (
	id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	nome VARCHAR(50) NOT NULL,
	preco DECIMAL(10,2) NOT NULL,
    estoque_qty INTEGER DEFAULT 0
);
CREATE TABLE pedidos (
	id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
    total DECIMAL(10,2),
	dataPedido TIMESTAMP
);
CREATE TABLE pedidos_x_produtos (
	id INTEGER GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
	quantidade INTEGER NOT NULL,
	id_Pedido INTEGER NOT NULL,
	id_Produto INTEGER NOT NULL,
	FOREIGN KEY (id_pedido) REFERENCES pedidos(id),
	FOREIGN KEY (id_produto) REFERENCES produtos(id)
);