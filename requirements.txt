Django==2.0.9
django-firebird==2.0a0
fdb==2.0.0
future==0.17.1
pytz==2018.9
sqlparse==0.3.0
