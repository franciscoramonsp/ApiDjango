import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '2-*9$w(+*$i@#64wus5j@xf479ql(r9=mqn_jn7-216m^uqsay'

DEBUG = True

ALLOWED_HOSTS = []

CORE_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

LOCAL_APPS = [
    'rest'
]

INSTALLED_APPS = CORE_APPS + LOCAL_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ApiDjango.urls'

WSGI_APPLICATION = 'ApiDjango.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'firebird',
        'NAME': '/home/ramon/django.fdb',
        'USER': 'sysdba',
        'PASSWORD': '7c32cad1',
        'HOST': '127.0.0.1',
        'PORT': '3050',
        'OPTIONS': {'charset': 'UTF-8'}
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'pt-br'

TIME_ZONE = 'America/Belem'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
